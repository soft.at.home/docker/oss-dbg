FROM registry.gitlab.com/soft.at.home/docker/sah-nte:v1.4.9

ENV STAGINGDIR=""
ENV CONFIGDIR=""
ENV PACKAGEDIR=""

RUN apt-get clean && \
    apt-get update && \
    apt-get -y --no-install-recommends install \
    bison \
    chrpath  \
    diffstat  \
    doxygen \
    flex \
    gawk \
    git-lfs \
    libcap-ng-dev \
    libfcgi-dev \
    libmnl-dev \
    libssl-dev \
    liburiparser-dev \
    libuv1-dev \
    libxml2-dev \
    libxslt1-dev \
    libyajl-dev \
    lighttpd \
    locales \
    python-is-python3 \
    ssh \
    sudo \
    texinfo \
    tmux \
    zlib1g-dev

RUN echo "dash dash/sh boolean false" | debconf-set-selections && \
    DEBIAN_FRONTEND=noninteractive dpkg-reconfigure dash

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen && \
    update-locale LANG=en_US.UTF-8

ENV TERM xterm-256color
ENV LANG=C.UTF-8
COPY resources/ /
RUN chmod 644 /etc/sudoers

RUN cd ~

ENV LD_LIBRARY_PATH /usr/local/lib

RUN cd ~

RUN rm -rf /var/lib/apt/lists/* /var/tmp/* /tmp/*

RUN git clone git://git.openwrt.org/project/uci.git && \
    git clone git://git.openwrt.org/project/rpcd.git && \
    cd uci && cmake . && make && make install && \
    cd ../rpcd && cmake -DIWINFO_SUPPORT=OFF -DUCODE_SUPPORT=OFF . && make && make install && \
    cd .. && rm -rf uci rpcd

RUN cd ~

RUN git clone https://github.com/rdkcentral/rbus.git && \
    cd rbus && mkdir build && cd build && \
    cmake -DBUILD_RTMESSAGE_LIB=ON -DBUILD_RTMESSAGE_SAMPLE_APP=ON -DBUILD_FOR_DESKTOP=ON \
          -DBUILD_DATAPROVIDER_LIB=ON -DCMAKE_BUILD_TYPE=release \
          -DCMAKE_C_FLAGS="-Wno-stringop-truncation -Wno-stringop-overflow" .. && \
    make && make install && \
    cd ../../ && rm -rf rbus

RUN curl https://storage.googleapis.com/git-repo-downloads/repo > ~/repo && \
    chmod a+x ~/repo && \
    chown 0:0 ~/repo && \
    mv ~/repo /usr/bin/repo

RUN git clone https://github.com/warmcat/libwebsockets.git && \
    cd libwebsockets && git checkout v4.2.2 && mkdir build && cd build && cmake .. -DLWS_WITH_LIBUV=1 && \
    make && make install && cd ../../ && rm -rf libwebsockets

RUN git clone https://github.com/tsl0922/ttyd.git && \
    cd ttyd && mkdir build && cd build && cmake .. && make && make install && cd .. && \
    cd .. && rm -rf ttyd

RUN pip3 install gdbgui

RUN luarocks install penlight

# Reset to default socket in development container
ENV PCB_SYS_BUS ""

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["/usr/local/bin/init.sh"]
