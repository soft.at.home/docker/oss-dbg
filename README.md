# Debug & development environment

This docker container image provides a debug & development environment for SoftAtHome components. A prebuild version can be found at registry.gitlab.com/soft.at.home/docker/oss-dbg:latest.

## Table of contents

[[_TOC_]]

## Set-up debug & development environment

### Setting up a local workspace

A workspace is a directory on your local disk. This can be any directory, preferable one dedicated to your project. This will be shared between your container and host system.

```
mkdir ~/projectdir
```

#### Create your debug and development container

It is possible to install all the tools needed on your local system, but better is to use a container with all the tools you need pre-installed. 

The docker container image provides everything you need already installed. To use a container image, make sure that the docker engine is installed.

If you do not know how to do this, here some links that can help you:

- [Get Docker Engine - Community for Ubuntu](https://docs.docker.com/engine/install/ubuntu/)
- [Get Docker Engine - Community for Debian](https://docs.docker.com/engine/install/debian/)
- [Get Docker Engine - Community for Fedora](https://docs.docker.com/engine/install/fedora/)
- [Get Docker Engine - Community for CentOs](https://docs.docker.com/engine/install/centos/)

Make sure you user id is added to the docker group:

```
sudo usermod -aG docker $USER
```

When the docker engine is installed you can create your debug and development container:

```
docker pull registry.gitlab.com/soft.at.home/docker/oss-dbg:latest
docker run -ti -d --name oss-dbg --restart always --cap-add=SYS_PTRACE --sysctl net.ipv6.conf.all.disable_ipv6=1 -e "USER=$USER" -e "UID=$(id -u)" -e "GID=$(id -g)" -v ~/projectdir:/home/$USER/projectdir registry.gitlab.com/soft.at.home/docker/oss-dbg:latest
```

After the above commands you container is launched and running in background. When your system reboots, the container is automatically restarted. Your "workspace" is mounted into the container and your user id is the same as on your host system.

#### Open a terminal to your container

You can open as many terminals/consoles as you like.

```
docker exec -ti oss-dbg /bin/bash
```

By default the user selected in the console is the container "root" user (which is not the same as your host system root user).

When writing or editing files in the "shared" workspace directory using this user, you need to be root as well on your host system to be able to open or edit these files.This can be very annoying as editing the source code is normally done using your favorite editor on your host.

You can change the user by using the "su" command in the container terminal.

```
su <USER ID>
```

Alternatively, you can pass the --user flag to the docker exec command.
To exec as the current user:

```
docker exec -ti --user $USER oss-dbg /bin/bash
```

#### Configure your development container

This is the last step to make the container you have created usable.

##### Gitlab configuration

The container should have access to Gitlab. You could share the private/public keys from your host system, but this is not recommended. It is better to create a new pair of keys.

Make sure that you are not "root" in the container (use "su" to change to your user id)

```
ssh-keygen -N "" -t rsa -b 2048 -C "<YOUR EMAIL HERE>" -f ~/.ssh/id_rsa
```

The above command will generate two files in "~/.ssh/":

1. id_rsa
2. id_rsa.pub

The first one contains your private key (for the container) and the second one the public key (for the container)

The content of your public key must be added to your Gitlab account. If you have no clue how to do this, please read  "[Adding an SSH key to your GitLab account
](https://docs.gitlab.com/ee/ssh/#adding-an-ssh-key-to-your-gitlab-account)"

##### Git configuration

In the container (terminal/console) configure some git global settings. Let git know who you are:

```
git config --global user.email "<YOUR EMAIL HERE>"
git config --global user.name "<YOUR FULL NAME HERE>"
```

#### Summary

When you finished this chapter you 

1. should have a docker container running with the name "oss-dbg"
2. should have a local workspace (directory `~/projectdir`) which is available in the container (directory `/home/<YOUR USER ID>/projectdir`)
3. can open multiple terminals to the container
4. can switch the container user (default = "root") to your own user id
5. can access Gitlab using the git command

The container image is based on debian buster. 

### Ambiorix repository

Now that the docker container is created and running it is time to fetch some sources from Gitlab. This is an example for the ambiorix project.

As the Ambiorix project has many sub-projects (libraries, examples, applications, ...) a google repo config is available, which will make it easier to work with them.

#### Fetch the Gitlab repositories

Make sure that you have a terminal open to the "oss-dbg" container and that you switch the user to your user id.

```
mkdir ~/projectdir/ambiorix
cd ~/projectdir/ambiorix
repo init -u git@gitlab.com:prpl-foundation/components/ambiorix/ambiorix.git
repo sync
repo forall -c "git checkout master"
```

If you are not familiar with the repo tool you can read more about it in the [online reference](https://source.android.com/setup/develop/repo#top_of_page).

If all goes well you should have all "ambiorix" Gitlab repositories under the directory "~/projectdir/ambiorix" and of each Gitlab repository the master branch selected.

Now that you have all Gitlab repositories available you can (in each directory of the Gitlab repositories):
- use the git commands to create branches, push/pull changes, ....
- build the repository (using "make")
- install the artifacts (using "sudo make install")
- run the tests (using "make test")

#### Building and installing

All components of the Ambiorix project compile out of source tree.

That is all intermediate files (`*.o`, `*.d`, `*.gcdna`, ....) are not put next to the source, but in a separate directory.

This directory is called "output". An extra sub-directory is created for each compile target (machine). This makes it possible to compile the sources with different toolchains.

By default there is only one toolchain installed "x86_64-linux-gnu". Unless you install a different toolchain and set the correct environment variables, all intermediate files and the compilation artifacts are put in the directory "./output/x86_64_linux-gnu".

To make development more comfortable, the `debug & development` container contains some [local commands](https://gitlab.com/soft.at.home/docker/oss-dbg/-/blob/main/resources/usr/local/bin/local_commands.sh). 

You can simply build all ambiorix components by executing:

```
amx_rebuild_all
```

##### Ambiorix Libraries

The libraries must be compiled and installed in a specific order. Some of them depend on other libraries. You can find a dependency graph in the [`getting started`](https://gitlab.com/prpl-foundation/components/ambiorix/tutorials/getting-started/-/blob/main/README.md#manual) tutorial.

You can manually install a library by executing the following commands:

```
make
sudo -E make install
```

Or use the predefined command to install all of them at once:

```
amx_rebuild_libs
```

##### Bus back-ends

The order of building and installing the backends is not of importance.

You can manually install a backend by executing:

```
make
sudo -E make install
```

Or use the predefined command to install all of them at once:

```
amx_rebuild_bus_backends
```

##### Applications

You can manually install an application by executing:

```
make
sudo -E make install
```

Or use the predefined command to install all of them at once:

```
amx_rebuild_apps
```

##### Examples

You can manually install an example by executing:

```
make
sudo -E make install
```

Or use the predefined command to install all of them at once:

```
amx_rebuild_examples
```

#### Testing

Most of the Ambiorix Gitlab repo's contain (unit) tests. Currently the applications do not have tests available.

When changing code it is recommended that you:

1. run the existing tests
2. add tests to test your changes

Be aware that whenever you need to change existing tests, you probably broke backwards compatibility.

##### Run tests

To run a test for a specific Gitlab repo, all you need to do is cd in that repo and run the tests.


Example:

```
cd ~/projectdir/ambiorix/libraries/libamxc
make test
```

You could run all tests for all libraries at once - just be sure that you already compiled all libraries and installed them.

```
cd ~/projectdir/ambiorix
repo forall libraries/* -c "make test"
```

##### Open coverage reports

When tests have been run, test code coverage reports are generated. These reports are available in the output directory (default = ./output/x86_64_linux-gnu/coverage/report).

These reports are in HTML format. If the output directory is on the shared filesystem with the host system, you can directly open the reports on the host system:

```
cd ~/ambiorix/libraries/libamxc/output/x86_64-linux-gnu/coverage/report/
xdg-open index.html
```

If the output directory is not on the shared filesystem, you can run a HTTP server in the container:

```
cd ~/projectdir
python3 -m http.server 8080
```

You could open a separate terminal to the container and run the HTTP server in that terminal or run the HTTP server in background and redirect all output to "/dev/null".

When the HTTP server is started you can access the coverage reports using your browser on the host system.

The url is typically (unless you use different names):

```
http://172.17.0.2:8080/ambiorix/libraries/libamxc/output/x86_64-linux-gnu/coverage/report/
```
The IPv4 address is the IPv4 address of your container. 

#### Debugging

Any application or test suite can be debugged using gdb or  - if you prefer a graphical user interface frontend for gdb - gdbgui.

gdb documentation is available at: https://www.gnu.org/software/gdb/documentation/

More information about gdbgui is available at: https://www.gdbgui.com/

Example:

Debugging a test case using gdbgui.

All you need to do is build the test suite, for this example let's debug the test suite "amxo_include" from libamxo (the odl parser).

First make sure you can build and run the tests of libamxo:

```
cd ~/projectdir/ambiorix/libraries/libamxo
make test
```

Change the directory to the test directory and start the test using gdbgui:

```
cd tests/amxo_include/
gdbgui -r ./run_test
```

As a response you should see some output of gdbgui:

```
Opening gdbgui with default browser at http://127.0.0.1:5000
exit gdbgui by pressing CTRL+C
```

Now open a browser (or a new tab) and open the uri "http://127.0.0.1:5000"

Now you should see the gdbgui user interface in your browser, and you can start debugging.

If the interface is not clear immediately, please read the manual.

#### Summary

When you finished this chapter you

1. are able to build and install all components
2. you can run tests
3. you can access the coverage reports in your browser
4. you can start a debugging session with gdbgui

