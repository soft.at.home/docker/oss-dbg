# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.2.1 - 2024-03-26(12:17:34 +0000)

### Other

- Revert to old Docker image tag syntax

## Release v1.2.0 - 2023-09-01(11:29:16 +0000)

### New

- Add RUST Support in CI

### Fixes

- Pylint issues with python3 bindings tests
- Fix Python3 amx bindings install target

### Other

- [SID] Tests from version v1.3.0
- [Ambiorix][Docker] Add libmnl-dev to amx-sah-dbg
- Change python3 bindings tests to comply with usp spec
- Add gitlabinternal to known_hosts

## Release v1.1.0 - 2023-02-07(10:12:14 +0000)

### New

- Add libuspi to USP install command

### Fixes

- Path to sut.dm.IP.Interface['3'].IPv4Address['1'].proxy() doenst work
- [amx] Unable to get data model object after a reboot

### Other

- * Add acl group to container

## Release v1.0.15 - 2022-09-02(06:50:57 +0000)

### Fixes

- Issue: ambiorix/bindings/python3#14 Issues with datamodel during tests

## Release v1.0.14 - 2022-08-26(14:16:45 +0000)

## Release v1.0.13 - 2022-08-17(14:02:45 +0000)

## Release v1.0.12 - 2022-08-01(11:40:18 +0000)

## Release v1.0.11 - 2022-08-01(06:34:22 +0000)

### Other

- sysinit done optional

## Release v1.0.10 - 2022-07-19(06:55:37 +0000)

## Release v1.0.9 - 2022-07-15(10:36:42 +0000)

## Release v1.0.8 - 2022-07-12(16:10:15 +0000)

## Release v1.0.7 - 2022-07-12(09:11:59 +0000)

### Other

- USP needs async userflags for functions

## Release v1.0.6 - 2022-07-04(15:34:49 +0000)

### Fixes

- Keep ssh key in root directory

### Other

- Upstep container to use latest versions of pcb

## Release v1.0.5 - 2022-04-28(15:15:20 +0000)

### Fixes

- It is no longer possible to run the container with your user id
- PCB_SYS_BUS variable points to non-existing socket
- Only remove ssh key in internal image

## Release v1.0.4 - 2022-04-14(14:11:15 +0000)

### Other

- Upstep docker base to sah-nte:v1.1.14

## Release v1.0.3 - 2022-04-14(12:51:48 +0000)

### Other

- Debug containers starts from sah-nte container

## Release v1.0.1 - 2022-01-20(10:56:42 +0000)

## Release v0.0.0 - 2021-05-19(08:25:16 +0000)

### New

- Initial release
