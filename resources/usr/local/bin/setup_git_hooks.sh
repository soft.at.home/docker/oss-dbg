#!/bin/bash

if [ -n "$USER" ]; then
    mkdir -p /home/"$USER"/.git/hooks
    cp /etc/git/hooks/pre-push /home/"$USER"/.git/hooks/

    echo -e "[core]\n\thooksPath = /home/\"$USER\"/.git/hooks" > /home/"$USER"/.gitconfig
fi
