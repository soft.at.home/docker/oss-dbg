#!/bin/bash
if ! getent passwd "$USER"; then
    create_user_env.sh
fi
if [ ! -f /home/"$USER"/.gitconfig ]; then
    setup_git_hooks.sh
fi
open_user_shell.sh
