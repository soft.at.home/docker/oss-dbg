#!/bin/bash
if [ -n "$USER" ]; then
    USER_UID=$UID
    if [ -z "$USER_UID" ]; then
        USER_UID=1000
    fi
    if [ $USER_UID == 0 ]; then
        USER_UID=1000
    fi
    if [ -z "$GID" ]; then
        GID=1000
    fi

    groupadd --gid "$GID" "normal_users"
    useradd --home /home/"$USER" --uid "$USER_UID" --gid "$GID" --shell /bin/bash "$USER"
    usermod -a -G sudo "$USER"
    chown "$USER_UID":"$GID" /home/"$USER"
    if [ ! -x /home/"$USER"/.bashrc ]; then
        cp /etc/skel/.bashrc /home/"$USER"
        chown "$USER_UID":"$GID" /home/"$USER"/.bashrc
    fi

    echo "source /usr/local/bin/local_commands.sh" >> /home/"$USER"/.bashrc

    if [ ! -x /home/"$USER"/.bash_logout ]; then    
        cp /etc/skel/.bash_logout /home/"$USER"
        chown "$USER_UID":"$GID" /home/"$USER"/.bash_logout
    fi
   
    if [ -n "$OWNS" ]; then
        for dir in $OWNS; do
            chown -R "$USER_UID":"$GID" "$dir"
        done
    fi
fi
