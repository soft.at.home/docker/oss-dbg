#!/bin/bash

# take tag of current lib
tag=$(git describe --tags) || tag="v0.0.0"
tag=${tag#*_v}
tag=${tag%%-*}
echo $tag

# build version parts
IFS=. read -r major minor build <<<"${tag}"
echo "$major" - "$minor" - "$build"

# build name
name=${PWD##*/}
name=${name//_}

for i in "$@"; do
  echo update "$i"
  cd "$i" || exit
  sed -i -e "s/$name=[0-9]*\.[0-9]*\.[0-9]*/$name=$major.$minor.$build/g" .gitlab-ci.yml
  cat .gitlab-ci.yml
  cd - || exit
done;
